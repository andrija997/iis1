/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entiteti;
import IdClass.PustaoId;
import javax.persistence.*;
/**
 *
 * @author andri
 */
@Entity
@Table(name = "pustao")
@IdClass(PustaoId.class)
public class Pustao {
   @Id
   @ManyToOne
   @JoinColumn(name = "IdK",referencedColumnName = "Id")
   private Korisnik korisnik;
   
   @Id
   @ManyToOne
   @JoinColumn(name = "IdP", referencedColumnName = "Id")
   private Pesma pesma;

    public Pustao() {
    }
    
    public Pustao(Korisnik korisnik, Pesma pesma) {
        this.korisnik = korisnik;
        this.pesma = pesma;
    }
   
    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Pesma getPesma() {
        return pesma;
    }

    public void setPesma(Pesma pesma) {
        this.pesma = pesma;
    }
   
   
   
   
    
    
}
