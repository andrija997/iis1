/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entiteti;
import java.util.List;
import javax.persistence.*;
/**
 *
 * @author andri
 */

@Entity
@Table(name = "korisnik")
public class Korisnik {
    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    
    @OneToMany(mappedBy = "korisnik")
    List<Pustao> pustao;
    
    @JoinTable(
       name = "pustao",
            joinColumns = @JoinColumn(name = "IdK", referencedColumnName = "Id"),
            inverseJoinColumns = @JoinColumn(name = "IdP", referencedColumnName = "Id")
    )
    List<Pesma> pustaoPesme;

@Column(nullable = false,length = 25)
   private String ime;
@Column(nullable = false,length = 25)
   private String prezime;





    public Korisnik(int Id, List<Pustao> pustao, List<Pesma> pustaoPesme) {
        this.Id = Id;
        this.pustao = pustao;
        this.pustaoPesme = pustaoPesme;
    }

    
    public Korisnik() {
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }
    
    
    
    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public List<Pustao> getPustao() {
        return pustao;
    }

    public void setPustao(List<Pustao> pustao) {
        this.pustao = pustao;
    }

    public List<Pesma> getPustaoPesme() {
        return pustaoPesme;
    }

    public void setPustaoPesme(List<Pesma> pustaoPesme) {
        this.pustaoPesme = pustaoPesme;
    }
    
    
    
    
}
