/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entiteti;

import java.util.List;
import javax.persistence.*;
/**
 *
 * @author andri
 */
@Entity
@Table ( name = "pesma" )
public class Pesma {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    
    @Column(nullable = false, name = "naziv")
    private String naziv;
    
    @OneToMany(mappedBy = "pesma")
    private List<Pustao> pustao;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public List<Pustao> getPustao() {
        return pustao;
    }

    public void setPustao(List<Pustao> pustao) {
        this.pustao = pustao;
    }
    
    
}
