package Entiteti;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T13:44:48")
@StaticMetamodel(Obaveza.class)
public class Obaveza_ { 

    public static volatile SingularAttribute<Obaveza, Long> datum;
    public static volatile SingularAttribute<Obaveza, String> kraj;
    public static volatile SingularAttribute<Obaveza, Long> vreme;
    public static volatile SingularAttribute<Obaveza, Boolean> alarm;
    public static volatile SingularAttribute<Obaveza, Integer> id;
    public static volatile SingularAttribute<Obaveza, String> pocetak;
    public static volatile SingularAttribute<Obaveza, String> text;
    public static volatile SingularAttribute<Obaveza, Integer> idAlarm;

}