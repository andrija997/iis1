
package korisnickiuredjaj;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import ObjekatPoruke.Poruka;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;
import javax.jms.*;


public class KorisnickiUredjaj {
    
    @Resource(lookup = "jms/__defaultConnectionFactory")
    static ConnectionFactory connectionFactory;
    
    @Resource(lookup = "MyTopic")
    static Topic topic;
    
    public static void main (String args[]){

JMSContext context = connectionFactory.createContext();

JMSProducer producer = context.createProducer();
   
JMSConsumer consumer = context.createConsumer(topic, "Id = 4", false);
   
   
    
    String inputUser;
    String currLocation;
    Scanner scanner = new Scanner(System.in);
    System.out.print("Uneti ime i prezime korisnika: ");
    inputUser = scanner.nextLine();
    System.out.println("\n Dobrodosli : " + inputUser);  
    System.out.print("Uneti trenutnu lokaciju: ");
    currLocation = scanner.nextLine();
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            java.util.Date date = new java.util.Date();
            String datetime = dateFormat.format(date);
            System.out.println("Current Date Time : " + datetime);
    while(true){
    System.out.println("0) Muzika: \n" +
                       "1) Alarm\n"+
                       "2) Planer\n"+
                       "3) Promena korisnika\n"+
                       "4) Promena adrese\n"+
                       "5) Kraj\n");
    
    int i = scanner.nextInt();
    scanner.nextLine();
    switch(i){
    case(0):  muzika(scanner, context, producer,inputUser,consumer); break;
    case(1):  alarm(scanner, context, producer,consumer); break;
    case(2):  planer(scanner, context, producer,currLocation,consumer); break;
    case(3):  inputUser = scanner.nextLine(); break;
    case(4):  currLocation = scanner.nextLine(); break;
       } 
    if (i == 5){
        ObjectMessage obj = context.createObjectMessage();
        Poruka kraj = new Poruka();
        kraj.setIdMetoda(9);
        try {
            obj.setObject(kraj);
            obj.setIntProperty("Id", 1);
            producer.send(topic,obj);
            obj = context.createObjectMessage(kraj);
            obj.setIntProperty("Id", 2);
            producer.send(topic,obj);
            obj = context.createObjectMessage(kraj);
            obj.setIntProperty("Id", 3);
            producer.send(topic,obj);
        } catch (JMSException ex) {
            Logger.getLogger(KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
    break;}
     }
   System.out.println("Dovidjenja  " + inputUser); 
    }
    
    private static long ymd(String s){
    String[] res = s.split("\\s+");
    return Integer.parseInt(res[0])*10000 + Integer.parseInt(res[1])*100 + Integer.parseInt(res[2]);
    }
   private static long hm(String s){
   String[] res = s.split("\\s+");
   return Integer.parseInt(res[3])*100 + Integer.parseInt(res[4]);
   }
   
    
    private static void muzika(Scanner scanner,JMSContext context, JMSProducer producer,String inputUser,JMSConsumer consumer){
     Poruka p = new Poruka();
     String[] splited = inputUser.split("\\s+");
     p.setIme(splited[0]).setPrezime(splited[1]);
        while(true){
    System.out.println("\n0) Pusti pesmu");
    System.out.println("1) Istorija muzike za korisnika");
    System.out.println("2) Nazad");
    int id = Integer.parseInt(scanner.nextLine());
    switch(id){
    case(0): System.out.print("Ime pesme: ");
             String pesma = scanner.nextLine();
             p.setPesma(pesma);
             p.setIdMetoda(0);
             posalji(p, 1, producer, context);
             break;
    case(1): p.setIdMetoda(1);
             posalji(p,1,producer,context); 
             Message message = consumer.receive();
             if(message instanceof ObjectMessage){
                    ObjectMessage objectMessage = (ObjectMessage) message;
        try {
            Poruka poruka = (Poruka) objectMessage.getObject();
            poruka.getLista().forEach((s) -> {
                System.out.println(s);
                        });
        } catch (JMSException ex) {
            Logger.getLogger(KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
       }
              break;
     case(2) : return;
      }
     }
    }
    
    
    
    
    private static void alarm(Scanner scanner,JMSContext context, JMSProducer producer,JMSConsumer consumer){
        Poruka p = new Poruka();
               while(true){
               System.out.println("\n0) Podesi alarm");
               System.out.println("1) Namesti da zvoni na periodu");
               System.out.println("2) Izaberi od ponudjenih");
               System.out.println("3) Postavi melodiju:");
               System.out.println("4) Izlistaj");
               System.out.println("5) Obrisi");
               System.out.println("6) Nazad");
               int i = Integer.parseInt(scanner.nextLine());
               switch(i){
               case(0):  System.out.print("Godina:");
                          int god = Integer.parseInt(scanner.nextLine());
                         System.out.print("\nMesec:");
                          int mesec = Integer.parseInt(scanner.nextLine());
                         System.out.print("\nDan:");
                          int dan = Integer.parseInt(scanner.nextLine());
                         System.out.print("\nSat:");
                          int sat = Integer.parseInt(scanner.nextLine());
                         System.out.print("\nMinut:");
                          int minut = Integer.parseInt(scanner.nextLine());
                          long date = god * 10000 + mesec*100 + dan;
                          long time = sat*100 + minut;
                          p.setDatumKraj(date).setVremeKraj(time).setIdMetoda(0);
                          posalji(p,2,producer,context);
                          break;
               case(1):   p.setIdMetoda(1);
                          posalji(p,2,producer,context);
                          break;                              
               case(2):   p.setIdMetoda(2);
                          posalji(p,2,producer,context);
                          Message msg = consumer.receive();
                          ObjectMessage obj = (ObjectMessage) msg;
                      try {
                          Poruka aux = (Poruka) obj.getObject();
                          ArrayList list = aux.getLista();
                          for (Object s : list) System.out.println(s);
                          System.out.print("Izbor od ponudjenih?(4 za nijedan) ");
                          int choice = Integer.parseInt(scanner.nextLine());
                          if (choice == 4) break;
                          p.setIdMetoda(0).setVremeKraj(hm((String)list.get(choice-1))).setDatumKraj(ymd((String)list.get(choice-1)));
                          posalji(p,2,producer,context);
                           } catch (JMSException ex) {
                           Logger.getLogger(KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
                             } break;               
               case(3):System.out.print("Uneti naziv melodije alarma: ");
                       String naziv = scanner.nextLine();
                       p.setPesma(naziv);
                       p.setIdMetoda(3);
                       posalji(p, 2, producer, context);
                         break;
               case(4): posalji(p.setIdMetoda(6),2,producer,context);
                        obj = (ObjectMessage) consumer.receive();
                         {
                            try {
                              p = (Poruka) obj.getObject();
                             
                               ArrayList<String> list = p.getLista();
                            for (String s: list) {
                                    System.out.println(s);
                                        }
                             } catch (JMSException ex) {
                                Logger.getLogger(KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
                               }
                          }
                         break;          
               case(5):System.out.print("Unos id alarma koja se brise: ");
                  int  id = Integer.parseInt(scanner.nextLine());
                   posalji(p.setIdPromene(id).setIdMetoda(5),2,producer,context); break;          
               case(6): return;               
               }
      }
    }
    
    
    
    private static void planer(Scanner scanner,JMSContext context, JMSProducer producer,String currLocation,JMSConsumer consumer){
   Poruka p = new Poruka();
        while(true){
    System.out.println("\n0) Dodaj obavezu");
    System.out.println("1) Izlistaj obaveze");
    System.out.println("2) Menjaj obavezu");
    System.out.println("3) Obrisi obavezu:");
    System.out.println("4) Razdaljinu izmedju dve adrese");
    System.out.println("5) Nazad");
    int i = Integer.parseInt(scanner.nextLine());
    switch(i){
    case(0): System.out.print("Godina:");
              int god = Integer.parseInt(scanner.nextLine());
             System.out.print("\nMesec:");
              int mesec = Integer.parseInt(scanner.nextLine());
             System.out.print("\nDan:");
              int dan = Integer.parseInt(scanner.nextLine());
             System.out.print("\nSat:");
              int sat = Integer.parseInt(scanner.nextLine());
             System.out.print("\nMinut:");
              int minut = Integer.parseInt(scanner.nextLine());
             System.out.print("\nOpis:");
              String opis = scanner.nextLine();
             System.out.print("\nDa li hocete da se budi alarm u potrebnom trenutku za datu adresu? ");
              boolean hocu = Boolean.parseBoolean(scanner.nextLine());
              if (hocu){
              System.out.print("\nUneti krajnju destinaciju: ");
              String kraj = scanner.nextLine();
              p.setHocuAlarm(hocu);
              p.setPocetak(currLocation); p.setKraj(kraj);
              }
             long date = god * 10000 + mesec*100 + dan;
             long time = sat*100 + minut;
             p.setPoruka(opis).setDatumKraj(date).setVremeKraj(time).setIdMetoda(0);
              posalji(p,3,producer,context);
        break;
    case(1): posalji(p.setIdMetoda(1),3,producer,context);
             ObjectMessage obj = (ObjectMessage) consumer.receive();
    {
        try {
            p = (Poruka) obj.getObject();
            if (p.getLista() == null) System.out.println("null");
            ArrayList<String> list = p.getLista();
            for (String s: list) {
                if (s == null) System.out.println("null");
            System.out.println(s);
            }
        } catch (JMSException ex) {
            Logger.getLogger(KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        break;
    case(2): System.out.print("Unos id poruke koja se menja: ");
              int id = Integer.parseInt(scanner.nextLine());
             System.out.print("\nGodina:");
              god = Integer.parseInt(scanner.nextLine());
             System.out.print("\nMesec:");
              mesec = Integer.parseInt(scanner.nextLine());
             System.out.print("\nDan:");
              dan = Integer.parseInt(scanner.nextLine());
             System.out.print("\nSat:");
              sat = Integer.parseInt(scanner.nextLine());
             System.out.print("\nMinut:");
              minut = Integer.parseInt(scanner.nextLine());
             System.out.print("Opis:");
              opis = scanner.nextLine();
              date = god * 10000 + mesec*100 + dan;
             time = sat*100 + minut;
             p.setPoruka(opis).setDatumKraj(date).setVremeKraj(time).setIdMetoda(2).setIdPromene(id);
              posalji(p,3,producer,context);
        break;
    case(3): System.out.print("Unos id poruke koja se brise: ");
               id = scanner.nextInt(); scanner.nextLine();
            posalji(p.setIdPromene(id).setIdMetoda(3),3,producer,context);
        break;
    case (4): System.out.print("\nMoze trenutna adresa?");
              Boolean moze = Boolean.parseBoolean(scanner.nextLine());
              String pocetna = currLocation;
              if (!moze){
              System.out.print("\nPocetna");
              pocetna = scanner.nextLine();
              }
              System.out.print("\nKrajnja adresa:");
              String krajnja = scanner.nextLine();
              p.setPocetak(pocetna); p.setKraj(krajnja);
              p.setIdMetoda(4);
              posalji(p,3,producer,context);
              obj = (ObjectMessage)consumer.receive();
    {
        try {
            Poruka aux = (Poruka)obj.getObject(); 
            int hours = (int) (aux.getVremeKraj()/ 3600);
            String s1=  String.format("%02d", hours);
            int minutes = (int) ((aux.getVremeKraj() % 3600) / 60);
            String s2=  String.format("%02d", minutes);
            int seconds = (int) (aux.getVremeKraj() % 60);
            String s3 =  String.format("%02d", seconds);
            System.out.println("Potrebno vreme: " + s1+ "h:" + s2 + "m:" + s3 + "s" );
        } catch (JMSException ex) {
            Logger.getLogger(KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
          break;   
    case(5):return;
      }
    
     }  
    }
     
    private static void posalji(Poruka p, int id,JMSProducer producer, JMSContext context){
    ObjectMessage obj = context.createObjectMessage();
        try {
            obj.setObject(p);
            obj.setIntProperty("Id", id);
            producer.send(topic,obj);
        } catch (JMSException ex) {
            Logger.getLogger(KorisnickiUredjaj.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }

}
