/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Planer;
import Entiteti.Obaveza;
import ObjekatPoruke.Poruka;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.TravelMode;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;


/**
 *
 * @author andri
 */
public class Planer extends Thread {
 private static final String API_KEY = "AIzaSyBegDEWd90NMqkJ7DMd0T0qUq9bIj6dk48";
 private static final GeoApiContext contextGeoApi = new GeoApiContext.Builder().apiKey(API_KEY).build();
  
    static ConnectionFactory connectionFactory;
    
    static Topic topic;
    
    static JMSConsumer consumer;
    static JMSProducer producer;
    static JMSContext context;
   private final EntityManager em;
 public static void main(String[] args) throws NamingException{
        final Properties initialContextProperties = new Properties();
        final String factory = "jms/__defaultConnectionFactory";
        final String topicName = "MyTopic";
        final InitialContext ic = new InitialContext(initialContextProperties);
        connectionFactory = (ConnectionFactory) ic
            .lookup(factory);
         topic = (Topic) ic.lookup(topicName);
     context = connectionFactory.createContext();
     context.setClientID("App3");
     consumer = context.createDurableConsumer(topic,"Prijava3","Id = 3",false);
     producer = context.createProducer();
     EntityManagerFactory emf = Persistence.createEntityManagerFactory("PlanerPU");
     EntityManager em = emf.createEntityManager();
     new Planer(em);
 
 
 }

 public Planer(EntityManager em){
 this.em = em;
 start();
 }
 
 
 
 public void run(){
     while(true){
 Message message = consumer.receive();
 if (message instanceof ObjectMessage){
 ObjectMessage objMsg = (ObjectMessage) message;
     try {
         Poruka por = (Poruka)objMsg.getObject();
         switch(por.getIdMetoda()){
         case(0): dodaj(por);break;
         case(1): ArrayList aux =izlistaj();
                  ObjectMessage obj = context.createObjectMessage();
                  Poruka p = new Poruka();
                  p.setLista(aux);
                  obj.setObject(p);
                  obj.setIntProperty("Id", 4);
                  producer.send(topic,obj);
                  break;
         case(2): promeni(por);break;
         case(3): izbrisi(por.getIdPromene());
                  break;
         case(4): long minutes = estimateRouteTime(por.getPocetak(),por.getKraj());
                  ObjectMessage msg = context.createObjectMessage();
                  Poruka poruka = new Poruka();
                  poruka.setVremeKraj(minutes);
                  msg.setIntProperty("Id", 4);
                  msg.setObject(poruka);
                  producer.send(topic,msg);
                  break;
         case(9): return;        
         }
     } catch (JMSException ex) {
         Logger.getLogger(Planer.class.getName()).log(Level.SEVERE, null, ex);
    }
   }
  }
 }
 private long ymd(String s){
 String[] res = s.split("\\s+");
 return Integer.parseInt(res[0])*10000 + Integer.parseInt(res[1])*100 + Integer.parseInt(res[2]);
 }
 private long hm(String s){
 String[] res = s.split("\\s+");
 return Integer.parseInt(res[3])*100 + Integer.parseInt(res[4]);
 }
   public void dodaj(Poruka p){ //ako je poruka.getKraj == null samo u planer
   Obaveza obaveza = new Obaveza();
   obaveza.setDatum(p.getDatumKraj());
   obaveza.setVreme(p.getVremeKraj());
   obaveza.setText(p.getPoruka()); 
   if (p.isHocuAlarm()){
   Poruka alarm = new Poruka();
   SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd HH mm");
   Date curDate;
       try {
           curDate = format.parse(parse(p.getDatumKraj(),p.getVremeKraj()));
           Calendar cal = Calendar.getInstance();
           cal.setTime(curDate);
           long l =estimateRouteTime(p.getPocetak(), p.getKraj());
           cal.add(Calendar.SECOND, (int) (l*-1));
           String s = format.format(cal.getTime());
           alarm.setDatumKraj(ymd(s));
           alarm.setVremeKraj(hm(s));
           alarm.setPoruka(p.getPoruka());
           alarm.setIdMetoda(4);
           ObjectMessage obj = context.createObjectMessage(alarm);
           obj.setIntProperty("Id", 2);
           producer.send(topic, obj);
           Message msg = consumer.receive();
           TextMessage txt = (TextMessage)msg;
           obaveza.setAlarm(true);
           obaveza.setIdAlarm(Integer.parseInt(txt.getText()));
       } catch (ParseException ex) {
           Logger.getLogger(Planer.class.getName()).log(Level.SEVERE, null, ex);
       } catch (JMSException ex) {
           Logger.getLogger(Planer.class.getName()).log(Level.SEVERE, null, ex);
       }
   
   }
   synchronized(em){
   em.getTransaction().begin();
   em.persist(obaveza);
   em.getTransaction().commit();
   }
   }
   
   public ArrayList<String> izlistaj(){
   TypedQuery<Obaveza> query = em.createQuery("select o from Obaveza o order by o.datum,o.vreme ", Obaveza.class);
   List<Obaveza> list = query.getResultList();
   ArrayList<String> res = new ArrayList<String>();
   for(Obaveza o: list){
   int godina = (int) (o.getDatum()/10000);
   int mesec = (int)(o.getDatum()/100)%100;
   int dan = (int)(o.getDatum()%100);
   int sat = (int)(o.getVreme()/100);
   int minut = (int) (o.getVreme()%100);
   System.out.println(o.getId() +".  "+String.format("%02d", dan) + "." + String.format("%02d", mesec) + "."+godina + "   "+ String.format("%02d", sat) + ":"+ String.format("%02d", minut));
   System.out.println(o.getText());
   res.add(o.getId() +".  "+String.format("%02d", dan) + "." + String.format("%02d", mesec) + "."+godina + "   "+ String.format("%02d", sat) + ":"+ String.format("%02d", minut) + "\n" + o.getText());
   }
   return res;
   }
   public void izbrisi(int id){
       synchronized(em){
   Obaveza ob = em.find(Obaveza.class, id);
   if (ob!=null){
       em.getTransaction().begin();
       em.remove(ob);
       em.getTransaction().commit();
    if(ob.isAlarm()){
   ObjectMessage obj = context.createObjectMessage();
   Poruka p = new Poruka();
   p.setIdPromene(ob.getIdAlarm());
       try {
            p.setIdMetoda(5);
           obj.setIntProperty("Id", 2);
           obj.setObject(p);
           producer.send(topic,obj);
           System.out.println("eeee");
       } catch (JMSException ex) {
           Logger.getLogger(Planer.class.getName()).log(Level.SEVERE, null, ex);
       }
    }}}}
  
   public void promeni(Poruka p){
   synchronized(em){
   Obaveza ob = em.find(Obaveza.class,p.getIdPromene());
   if (ob!=null){
    em.getTransaction().begin();
   ob.setDatum(p.getDatumKraj());
   ob.setVreme(p.getVremeKraj());
   ob.setText(p.getPoruka());
   em.getTransaction().commit();
   }
    }
   }
   
   public static long estimateRouteTime(String start, String end) {
    try {
        DistanceMatrixApiRequest req = DistanceMatrixApi.newRequest(contextGeoApi);
       
        
        DistanceMatrix result = req.origins(start)
                .destinations(end)
                .mode(TravelMode.DRIVING)
                .language("en-US")
                .await();
        return  result.rows[0].elements[0].duration.inSeconds;
    // return result.rows[0].elements[0].distance.inMeters;

    } catch (ApiException e) {
        System.out.println(e.getMessage());
    } catch (IOException | InterruptedException e) {
        System.out.println(e.getMessage());
    }
    return -1; //something went wrong
    
}
       private String parse(long datum,long vreme){
    int dan = (int) (datum %100);
    int mesec = (int) ((datum/100)%100);
    int godina = (int) (datum/10000);
    int sat = (int) (vreme/100);
    int minut = (int) (vreme%100);
    return new String(godina+" "+mesec+" "+dan+" "+sat+" "+minut);
    }
   
}
