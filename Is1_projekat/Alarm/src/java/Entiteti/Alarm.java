/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entiteti;
import java.util.Date;
import javax.persistence.*;
/**
 *
 * @author andri
 */
@Entity
public class Alarm {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(nullable = true)
    private String opis;
    @Column(nullable = true)
    private boolean periodicni;

    public boolean isPeriodicni() {
        return periodicni;
    }

    public Alarm setPeriodicni(boolean periodicni) {
        this.periodicni = periodicni;
        return this;
    }

    public int getId() {
        return id;
    }

    public Alarm setId(int id) {
        this.id = id;
        return this;   
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

   
    public String getOpis() {
        return opis;
    }

    public Alarm setOpis(String opis) {
        this.opis = opis;
        return this;
    }
    
    
    
    
    
    
}
