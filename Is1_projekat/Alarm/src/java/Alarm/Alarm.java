/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alarm;

import ObjekatPoruke.Poruka;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.persistence.EntityManager;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
/**
 *
 * @author andri
 */
public class Alarm extends Thread {
    static ConnectionFactory connectionFactory;
    static Topic topic;
    
    static JMSConsumer consumer;
    static JMSProducer producer;
    static JMSContext context;
    
    private final EntityManager em;
    static EntityManagerFactory emf;
    private int perioda = 30000; //in MiliSeconds
    private Zvono zvono;
    
   public static void main(String[] args) throws NamingException{
        final Properties initialContextProperties = new Properties();
        final String factory = "jms/__defaultConnectionFactory";
        final String topicName = "MyTopic";
        final InitialContext ic = new InitialContext(initialContextProperties);
        connectionFactory = (ConnectionFactory) ic
            .lookup(factory);
         topic = (Topic) ic.lookup(topicName);
     context = connectionFactory.createContext();
     context.setClientID("App2");
     consumer = context.createDurableConsumer(topic,"Prijava2","Id = 2",false);
     producer = context.createProducer();
    emf = Persistence.createEntityManagerFactory("AlarmPU");
    EntityManager em = emf.createEntityManager();
     new Alarm(em);
    }
    public Alarm(EntityManager em){
    this.em = em;
    start();
     zvono = new Zvono(em);
    }
    
  @Override
    public void run(){
        propusteniAlarm();
        zvono.start();
     while(true){
 Message message = consumer.receive();
 if (message instanceof ObjectMessage){
 ObjectMessage objMsg = (ObjectMessage) message;
     try {
         Poruka por = (Poruka)objMsg.getObject();
       
         switch(por.getIdMetoda()){
         case(0): navij(por.getDatumKraj(),por.getVremeKraj()); break;
         case(1): navijPeriodu(); break;
         case(2): ArrayList list = ponudi();
                  objMsg = context.createObjectMessage();
                  objMsg.setIntProperty("Id", 4);
                  Poruka poruka = new Poruka();
                  objMsg.setObject(por.setLista(list));
                  producer.send(topic,objMsg);
                  break;
         case(3): postaviMelodiju(por.getPesma()) ;break;
         case(4): int id = navij(por.getDatumKraj(),por.getVremeKraj(),por.getPoruka());
                  TextMessage txt = context.createTextMessage(""+id);
                  txt.setIntProperty("Id", 3);
                  producer.send(topic, txt);
                  break;
         case(5): izbrisi(por.getIdPromene()); break;
         case(6): ArrayList aux =izlistaj();
                  ObjectMessage obj = context.createObjectMessage();
                  Poruka p = new Poruka();
                  p.setLista(aux);
                  obj.setObject(p);
                  obj.setIntProperty("Id", 4);
                  producer.send(topic,obj);
                  break;      
         case(9) : zvono.interrupt(); return;
         }
     } catch (JMSException ex) {
         Logger.getLogger(Alarm.class.getName()).log(Level.SEVERE, null, ex);
    }
   }
  }
    }
    public void izbrisi(int id){
    synchronized(em){
    em.getTransaction().begin();
    Entiteti.Alarm EA  = em.find(Entiteti.Alarm.class, id);
    if (EA != null)
    em.remove(EA);
    em.getTransaction().commit();
    }    
    }
    public ArrayList<String> izlistaj(){
   TypedQuery<Entiteti.Alarm> query = em.createQuery("select o from Alarm o order by o.date ", Entiteti.Alarm.class);
   List<Entiteti.Alarm> list = query.getResultList();
   ArrayList<String> res = new ArrayList<String>();
    SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm");
   for(Entiteti.Alarm o: list){
   res.add(o.getId() + ".  " + format.format(o.getDate()));
   }
   return res;
   }
    
    
    
    private String parse(long datum,long vreme){
    int dan = (int) (datum %100);
    int mesec = (int) ((datum/100)%100);
    int godina = (int) (datum/10000);
    int sat = (int) (vreme/100);
    int minut = (int) (vreme%100);
    return new String(godina+" "+mesec+" "+dan+" "+sat+" "+minut);
    }
    
     public void navij(long datum, long vreme){ //assuming that my parameters are fine
         Entiteti.Alarm alarm = new Entiteti.Alarm();
         SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd HH mm");
        try {
            Date curDate = format.parse(parse(datum,vreme)); 
            alarm.setDate(curDate);
            synchronized(em){
            em.getTransaction().begin();
            em.persist(alarm);
            em.getTransaction().commit();}
        } catch (ParseException ex) {
            Logger.getLogger(Alarm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public int navij(long datum, long vreme ,String opis){ 
        
         Entiteti.Alarm alarm = new Entiteti.Alarm();
         SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd HH mm");
        try {
            Date curDate = format.parse(parse(datum,vreme)); 
            alarm.setDate(curDate);
            alarm.setOpis(opis);
            synchronized(em){
            em.getTransaction().begin();
            em.persist(alarm);
            em.getTransaction().commit();}
        } catch (ParseException ex) {
            Logger.getLogger(Alarm.class.getName()).log(Level.SEVERE, null, ex);
        }
        return alarm.getId();
    }
     public void navijPeriodu(){
     Calendar cal = Calendar.getInstance();
     cal.setTime(new Date());
     cal.add(Calendar.MILLISECOND,perioda);
     Entiteti.Alarm alarm = new Entiteti.Alarm();
     alarm.setDate(cal.getTime());
     alarm.setPeriodicni(true);
     synchronized(em){
      em.getTransaction().begin();
      em.persist(alarm);
      em.getTransaction().commit();
      }
     }
     
     public void ugasiPeriodu(){
     synchronized(em){
      
         TypedQuery<Entiteti.Alarm> query = em.createQuery("select s from Alarm s where s.periodicni = true",Entiteti.Alarm.class);
      List<Entiteti.Alarm> list = query.getResultList();
      for (Entiteti.Alarm a : list){
          em.getTransaction().begin();
      em.remove(a); 
      em.getTransaction().commit();
      }
     
     }   
     }
     
     public int getPerioda() {
        return perioda;
    }

     public Alarm setPerioda(int perioda) {
        this.perioda = perioda;
        return this;
    }
   
     
    public ArrayList<String> ponudi(){
     Calendar cal = Calendar.getInstance();
     cal.setTime(new Date());
     
    ArrayList<String> dateList = new ArrayList();
    SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd HH mm");
    cal.add(Calendar.MILLISECOND,perioda);
    dateList.add(format.format(cal.getTime()));
    cal.add(Calendar.MILLISECOND,perioda);
    dateList.add(format.format(cal.getTime()));
    cal.add(Calendar.MILLISECOND,perioda);
    dateList.add(format.format(cal.getTime()));
    return dateList;
    }     
     private long ymd(String s){
 String[] res = s.split("\\s+");
 return Integer.parseInt(res[0])*10000 + Integer.parseInt(res[1])*100 + Integer.parseInt(res[2]);
 }
   private long hm(String s){
   String[] res = s.split("\\s+");
   return Integer.parseInt(res[3])*100 + Integer.parseInt(res[4]);
  }
    private void propusteniAlarm(){
    synchronized(em){
          Date date = new Date();
          SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd HH mm");
          TypedQuery query = em.createQuery("SELECT a FROM Alarm a", Entiteti.Alarm.class);
          List<Entiteti.Alarm> lista = query.getResultList();
          em.getTransaction().begin();
          for(Entiteti.Alarm a: lista){
              if (a.getDate().before(date)){
              System.out.println("Propusten alarm " + format.format(a.getDate()));
              em.remove(a);}
          }
          em.getTransaction().commit();
          }
    }    
    
    private class Zvono extends Thread {
   EntityManager em;
    Zvono(EntityManager em){
        
        this.em = em;
    }
    public void run(){
       Entiteti.Alarm alarm;
       Calendar cal = Calendar.getInstance();
       
       while(!interrupted()){
        synchronized(em){
          TypedQuery query = em.createQuery("SELECT a FROM Alarm a WHERE a.date = (SELECT MIN(a2.date) FROM Alarm a2)", Entiteti.Alarm.class);
          List<Entiteti.Alarm> lista = query.getResultList();
          Date date = new Date();
          cal.setTime(date);
          for(Entiteti.Alarm a: lista){
          if (date.compareTo(a.getDate())>= 0){
          zvoni();
          if(a.getOpis()!= null) System.out.println(a.getOpis()); 
          em.getTransaction().begin();em.remove(a);em.getTransaction().commit();
          if (a.isPeriodicni())navijPeriodu();
          }
          
          }
        }
           try {
               sleep(1000);
           } catch (InterruptedException ex) {
             return;
           }
       }
    }
    
    private void zvoni(){
    ObjectMessage obj = context.createObjectMessage();
    Poruka p = new Poruka();
        try {
            obj.setIntProperty("Id", 1);  
            p.setIme("Zvonko").setPrezime("Budilnik").setPesma(melodija);
            obj.setObject(p);
            producer.send(topic,obj);
        } catch (JMSException ex) {
            Logger.getLogger(Alarm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    }
    
    
    private static String melodija = "Budilnik";
    public void postaviMelodiju(String naziv){
        System.out.println(naziv);
    melodija = naziv;
}}
