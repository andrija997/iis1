/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjekatPoruke;

import java.io.Serializable;

import java.util.ArrayList;
/**
 *
 * @author andri
 */
public class Poruka implements Serializable {
    private String ime;
    private String prezime;
    private String pesma;
    private String pocetak;
    private String kraj;
    private long datumKraj;
    private long vremeKraj ;
    private String poruka;
    private int idPromene;
    private int idMetoda;
    private boolean hocuAlarm;
    private ArrayList<String> lista = null;

    public boolean isHocuAlarm() {
        return hocuAlarm;
    }

    public void setHocuAlarm(boolean hocuAlarm) {
        this.hocuAlarm = hocuAlarm;
    }

    
    public ArrayList getLista() {
        return lista;
    }

    public Poruka setLista(ArrayList lista) {
        this.lista = lista;
        return this;
    }
    
    
    public int getIdMetoda() {
        return idMetoda;
    }

    public Poruka setIdMetoda(int idMetoda) {
        this.idMetoda = idMetoda;
        return this;    
    }
    
    public int getIdPromene() {
        return idPromene;
    }

    public Poruka setIdPromene(int idPromene) {
        this.idPromene = idPromene;
        return this;
    }
    
    public String getPocetak() {
        return pocetak;
    }

    public Poruka setPocetak(String pocetak) {
        this.pocetak = pocetak;
        return this;
    }

    public String getKraj() {
        return kraj;
    }

    public Poruka setKraj(String kraj) {
        this.kraj = kraj;
        return this;
    }
    
    public String getIme() {
        return ime;
    }

    public Poruka setIme(String ime) {
        this.ime = ime;
        return this;
    }

    public String getPrezime() {
        return prezime;
    }

    public Poruka setPrezime(String prezime) {
        this.prezime = prezime;
        return this;
    }

    public String getPesma() {
        return pesma;
    }

    public Poruka setPesma(String pesma) {
        this.pesma = pesma;
        return this;
    }

    public long getDatumKraj() {
        return datumKraj;
    }

    public Poruka setDatumKraj(long datumKraj) {
        this.datumKraj = datumKraj;
        return this;
    }

    public long getVremeKraj() {
        return vremeKraj;
    }

    public Poruka setVremeKraj(long vremeKraj) {
        this.vremeKraj = vremeKraj;
        return this;
    }

    public String getPoruka() {
        return poruka;
    }

    public Poruka setPoruka(String poruka) {
        this.poruka = poruka;
        return this;
    }
    
    
    
    
}
