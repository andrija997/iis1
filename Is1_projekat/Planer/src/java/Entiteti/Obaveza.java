/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entiteti;

import java.util.Date;
import javax.persistence.*;
/**
 *
 * @author andri
 */
@Entity
public class Obaveza {
  @Id  
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  
  private long datum;
  
  private long vreme;
  @Column(nullable = true)
  private String kraj,pocetak;
  @Column(length = 512)
  private String text;
  
  private boolean alarm;
  private int idAlarm;

    public boolean isAlarm() {
        return alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    public int getIdAlarm() {
        return idAlarm;
    }

    public void setIdAlarm(int idAlarm) {
        this.idAlarm = idAlarm;
    }
  
    public long getDatum() {
        return datum;
    }

    public void setDatum(long datum) {
        this.datum = datum;
    }

    public long getVreme() {
        return vreme;
    }

    public void setVreme(long vreme) {
        this.vreme = vreme;
    }

    public String getPocetak() {
        return pocetak;
    }

    public void setPocetak(String pocetak) {
        this.pocetak = pocetak;
    }

    public String getKraj() {
        return kraj;
    }

    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
  
    
}
