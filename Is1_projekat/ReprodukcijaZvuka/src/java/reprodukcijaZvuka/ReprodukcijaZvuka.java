
package reprodukcijaZvuka;

import Entiteti.Korisnik;
import Entiteti.Pesma;
import Entiteti.Pustao;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import java.io.IOException;
import java.util.List;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;

import java.net.URI;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import javax.jms.*;

import ObjekatPoruke.Poruka;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.CustomsearchRequestInitializer;
import com.google.api.services.customsearch.model.Result;
import com.google.api.services.customsearch.model.Search;
import java.awt.Desktop;
import java.security.GeneralSecurityException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



public class ReprodukcijaZvuka extends Thread {
  
    static ConnectionFactory connectionFactory;
    
    static Topic topic;
    
    static JMSConsumer consumer;
    static JMSProducer producer;
    static JMSContext context;
    
    private static final String cx = "001157226002254462161:gmmh6mlrrim"; 
    private static final JsonFactory JSON_FACTORY =  JacksonFactory.getDefaultInstance();
    private final EntityManager em;
    
    public  static void main(String[] args) throws NamingException{
        final Properties initialContextProperties = new Properties();
        final String factory = "jms/__defaultConnectionFactory";
        final String topicName = "MyTopic";
        final InitialContext ic = new InitialContext(initialContextProperties);
        connectionFactory = (ConnectionFactory) ic
            .lookup(factory);
         topic = (Topic) ic.lookup(topicName);
     context = connectionFactory.createContext();
     context.setClientID("App1");
     consumer = context.createDurableConsumer(topic,"Prijava1","Id = 1",false);
     producer = context.createProducer();
     EntityManagerFactory emf = Persistence.createEntityManagerFactory("ReprodukcijaZvukaPU");
     EntityManager em = emf.createEntityManager();
     new ReprodukcijaZvuka(em);
    }
    
    
    
   public ReprodukcijaZvuka(EntityManager em)  {
      this.em = em;
      start();
   }
 
   @Override
   public void run(){
   while(true){
       Message message = consumer.receive();
            if(message instanceof ObjectMessage){
                    ObjectMessage objectMessage = (ObjectMessage) message;
           try {
             Poruka  poruka = (Poruka)objectMessage.getObject();
               switch(poruka.getIdMetoda()){
                   case(0):pusti(poruka.getPesma(),poruka.getIme(),poruka.getPrezime()); break;
                   case(1):ArrayList list = slusao(poruka.getIme(),poruka.getPrezime()); 
                   Poruka p = new Poruka();
                   p.setLista(list);
                   ObjectMessage obj = context.createObjectMessage(p);
                   obj.setIntProperty("Id", 4);
                   producer.send(topic, obj);
                   break;
                   case(9): return;
               }
           } catch (JMSException ex) {
               Logger.getLogger(ReprodukcijaZvuka.class.getName()).log(Level.SEVERE, null, ex);
               return;
           }
                                   
         } 
      }
   }
   
   
    public  void pusti(String upit,String ime, String prezime) { 
        try{
         if (upit == null) return;
       pustiNaYT(upit);
       upisi(upit,ime,prezime);
        }
        catch(GeneralSecurityException e){}
        catch(IOException e){}
    }

    private  void pustiNaYT(String upit) throws IOException, GeneralSecurityException{

        Customsearch cs = new Customsearch.Builder(GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance(), null)
                .setApplicationName("MyApplication")
                .setGoogleClientRequestInitializer(new CustomsearchRequestInitializer("AIzaSyA4ePfwBFeFQTWbjMWncJhRio3Ia6bOrj0"))
                .build();

        //Set search parameter
        Customsearch.Cse.List list = cs.cse().list(upit).setCx(cx);

        //Execute search
        Search result = list.execute();
        URI uri = null;
        if (result.getItems()!=null){
            for (Result ri : result.getItems()) {
                if (uri == null) {
                    uri = URI.create(ri.getLink());
                }
                System.out.println(ri.getTitle() + ", " + ri.getLink());
            }
        }


        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            
                desktop.browse(uri);
      }
    }
  
    private void upisi(String pesma, String ime, String prezime){
       Korisnik korisnik = null;
   try{
      TypedQuery<Korisnik> queryKorisnik = em.createQuery("SELECT k FROM Korisnik  k WHERE" +
               " k.ime = :ime AND k.prezime=:prezime",Korisnik.class);
               queryKorisnik.setParameter("ime", ime).setParameter("prezime", prezime);
               korisnik = queryKorisnik.getSingleResult();
   }
   catch(NoResultException nre){ 
    em.setFlushMode(FlushModeType.COMMIT);
    korisnik = new Korisnik();
    korisnik.setIme(ime);
    korisnik.setPrezime(prezime);
    synchronized(em){
    em.getTransaction().begin();
    em.persist(korisnik);
    em.getTransaction().commit();
    }
   }
   Pesma pQR = null;
   try{
   TypedQuery<Pesma> queryPesma = em.createQuery("SELECT p FROM Pesma as p WHERE p.naziv = :pesma", Pesma.class);
  pQR = queryPesma.setParameter("pesma", pesma).getSingleResult();
   }
   catch(NoResultException nre){
   pQR = new Pesma();
   pQR.setNaziv(pesma);
    synchronized(em){
    em.getTransaction().begin();
    em.persist(pQR);
    em.getTransaction().commit();
    }
   }
   try{
       
   TypedQuery<Pustao> queryPustao = em.createQuery("SELECT p FROM Pustao as p WHERE p.pesma = :pQR AND p.korisnik=:korisnik", Pustao.class);
   queryPustao.setParameter("pQR", pQR).setParameter("korisnik", korisnik).getSingleResult();}
   catch(NoResultException nre){
   Pustao pustao= new Pustao(korisnik,pQR);
    synchronized(em){
   em.getTransaction().begin();
   em.persist(pustao);
   em.getTransaction().commit();}
   }
   } 
   
   public  ArrayList<String> slusao(String ime, String prezime){
       Korisnik korisnik = null;
   try{
      TypedQuery<Korisnik> queryKorisnik = em.createQuery("SELECT k FROM Korisnik  k WHERE" +
               " k.ime = :ime AND k.prezime=:prezime",Korisnik.class);
               queryKorisnik.setParameter("ime", ime).setParameter("prezime", prezime);
               korisnik = queryKorisnik.getSingleResult();
   }
   catch(NoResultException nre){ 
       System.out.println("Nema takvog korisnika!");
       return null;
   }
   
   TypedQuery<Pesma> tq =em.createQuery("Select s.pesma from Pustao s "+
           "where s.korisnik = :korisnik ", Pesma.class);
    List<Pesma> pustanePesme  = null;
   try{
     pustanePesme =  tq.setParameter("korisnik", korisnik).getResultList();
   }
   catch(NoResultException e){
   return null;
   }
   ArrayList<String> imena = new ArrayList<>();
   for (Pesma p: pustanePesme){
   System.out.println(p.getNaziv());
   imena.add(p.getNaziv());
   }
   return imena;
   }
}
