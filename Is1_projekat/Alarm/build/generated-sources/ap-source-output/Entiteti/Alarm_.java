package Entiteti;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-24T18:19:53")
@StaticMetamodel(Alarm.class)
public class Alarm_ { 

    public static volatile SingularAttribute<Alarm, Date> date;
    public static volatile SingularAttribute<Alarm, Boolean> periodicni;
    public static volatile SingularAttribute<Alarm, Integer> id;
    public static volatile SingularAttribute<Alarm, String> opis;

}