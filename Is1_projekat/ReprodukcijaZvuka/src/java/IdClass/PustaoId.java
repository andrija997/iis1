/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IdClass;

import java.io.Serializable;

/**
 *
 * @author andri
 */
public class PustaoId implements Serializable{
    private int pesma;
    private int korisnik;

    public PustaoId(int pesma, int korisnik) {
        this.pesma = pesma;
        this.korisnik = korisnik;
    }

    
    
    
    public int getPesma() {
        return pesma;
    }

  

    public int getKorisnik() {
        return korisnik;
    }

    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.pesma;
        hash = 67 * hash + this.korisnik;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PustaoId other = (PustaoId) obj;
        return true;
    }
    
    
}
